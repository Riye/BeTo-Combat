# BeTo - Combat

This module adds some improvements to the automation of the combat.

## Before Attack Roll

It hooks in to the `midi-qol.preAttackRoll` and does the following:

* Check if the target can be hit with the weapon
    1. Get Weapon Type (Melee, Thrown, Ranged)
    1. Get Min and Max range of the weapon
    1. Loop all targets and calculate distance to target
    1. For Melee and Thrown weapons, check if the target is in close combat distance (see getReach)
    1. For Thrown and Ranged weapons, check if the target is within the Max range
    1. For Ranged, check if the target is not in close combat distance
* Remove Ammunition if needed
    1. Check if the target can be hit
    1. Check if the weapon uses ammunition
    1. Get the amount of different ammunition the actor has
        * 0 > Notification and abort attack
        * 1 > The count of the ammunition is decreased with 1
        * More > Not yet implemented
* Remove weapon if thrown
    1. Check if the target can be hit
    1. Check if the weapon is thrown (can be thrown and target is not in close combat distance)
    1. The hook `beto.combat.removeThrownWeapon` is called
    1. If all succeed, the count of the item is decreased
* Check Disadvantage
    1. Weapon is Thrown or Ranged
    1. Distance to Target is in the Long Range
    1. The roll will be with disadvantage
