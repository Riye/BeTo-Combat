'use strict';
(factory => {
    window.Hooks.once('beto.library.loaded', beto => {
        beto.getModule('combat.functions').then(combat => {
            beto.registerModule('combat.before-attack-roll', factory(beto, combat));
        });
    });
})((beto, combat) => {
    const getMinRange = item => item.data.data.range.long ? item.data.data.range.value : 0;
    const getMaxRange = item => item.data.data.range.long ? item.data.data.range.long : item.data.data.range.value;
    const canHitTarget = (actor, weaponType, distanceToTarget, maxRange) => {
        if (weaponType === 'ranged' && combat.inCloseCombat(actor, distanceToTarget)) {
            ui.notifications.warn('The target is too close');
            return false;
        }
        if (distanceToTarget > maxRange) {
            ui.notifications.warn('The target is out of range');
            return false;
        }
        return true;
    };
    const removeAmmunition = item => {
        const ammunition = item.actor.data.items.filter(inventoryItem => inventoryItem.type === 'consumable' && inventoryItem.data.data.consumableType === 'ammo');
        switch (ammunition.length) {
            case 0:
                ui.notifications.warn('You have no ammunition');
                return false;
            case 1:
                ammunition[0].data.data.quantity--;
                ammunition[0].update({ data: { quantity: ammunition[0].data.data.quantity } });
                return true;
            default:
                ui.notifications.error('Multiple ammunition is not yet implemented.');
                // TODO @JB ask for ammunition and restart the roll workflow
                // new MidiQOL.BetterRollsWorkflow(item.actor, canvas.tokens.controlled[0], workflow.targets, damageRoll, {flavor: "Divine Smite - Damage Roll (Radiant)", itemCardId: args[0].itemCardId})
                return false;
        }
    };

    Hooks.on('midi-qol.preAttackRoll', function (item, workflow) {
        const minRange = getMinRange(item);
        const maxRange = getMaxRange(item);
        const usesAmmunition = combat.usesAmmunition(item);
        const weaponType = combat.getWeaponType(item);

        workflow.targets.forEach(target => {
            const distanceToTarget = beto.calculateDistance(item.actor.token, target);

            if (!canHitTarget(item.actor, weaponType, distanceToTarget, maxRange)) {
                return false;
            }

            if (usesAmmunition && !removeAmmunition(item)) {
                return false;
            }

            if (combat.isThrown(item, distanceToTarget) && !Hooks.call('beto.combat.removeThrownWeapon', item)) {
                item.data.data.quantity--;
                item.update({ data: { quantity: item.data.data.quantity } });
            }

            if (weaponType !== 'melee' && distanceToTarget > minRange) {
                workflow.rollOptions.disadvantage = true;
            }
        });
    });

    return {};
});
