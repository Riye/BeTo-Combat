'use strict';
(factory => {
    Hooks.once('beto.library.loaded', beto => {
        beto.registerModule('combat.functions', factory(beto));
    });
})((beto) => {
    const combat = {
        canBeThrown: item => item.data.data.properties.thr,
        usesAmmunition: item => item.data.data.properties.amm,
        getWeaponType: item => combat.usesAmmunition(item) ? 'ranged' : combat.canBeThrown(item) ? 'throwable' : 'melee',
        inCloseCombat: (actor, distanceToTarget) => distanceToTarget <= beto.getReach(actor),
        isThrown: (item, distanceToTarget) => combat.canBeThrown(item) && !combat.inCloseCombat(item.actor, distanceToTarget),
    };
    return combat;
});
